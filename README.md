# PBfrontier - README #
---

### Overview ###

The **PBfrontier** script (Excel 2007 VBA) can create an **Efficient Frontier** curve from the historical return data of different financial assets.

### Screenshots ###

![PBfrontier - Settings](development/readme/pbfrontier1.png "PBfrontier - Settings")

![PBfrontier - Calculation](development/readme/pbfrontier2.png "PBfrontier - Calculation")

![PBfrontier - Efficient Frontier](development/readme/pbfrontier3.png "PBfrontier - Efficient Frontier")

![PBfrontier - Solutions](development/readme/pbfrontier4.png "PBfrontier - Solutions")

### Setup ###

* Open the file **PBfrontier.xlsm** in the Microsoft Excel 2007 application.
* Enable the execution of Excel macros in the settings of Microsoft Excel.
* Edit the settings on the first tab called **Settings**.
* Add historical return data on the tab called **Return Data**.
* Open the Excel calculation tab called **Calculations**.
* Start the calculation with the button **Create Efficient Frontier**.
* Check the result on the tabs **Efficient Frontier** and **Solutions**.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBfrontier** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
